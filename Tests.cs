using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace web_test.SeleniumTests;

public class Tests
{
    private static readonly string screenshotFolder = Directory.GetCurrentDirectory()+"/screenshots/";
    private static readonly Uri remoteDriverURI = new Uri(GetRemoteDriverURI());
    private static readonly DriverOptions driverOptions = GetDriverOptions();
    
    private static string GetRemoteDriverURI()
    {
        var uri = Environment.GetEnvironmentVariable("RemoteDriverURI");
        return Environment.GetEnvironmentVariable("RemoteDriverURI") ?? "http://localhost:4444";
    }


    private static DriverOptions MatchBrowser(string browser) => browser switch
    {
        "chrome" => new ChromeOptions(),
        "firefox" => new FirefoxOptions(),
        "edge" => new EdgeOptions(),
        null => new ChromeOptions(),
        _ => throw new ArgumentException($"Browser '{browser}' is not known"),
    };

    private static DriverOptions GetDriverOptions()
    {
        var browser = Environment.GetEnvironmentVariable("BrowserType");
        return MatchBrowser(browser);
    }

    [Test]
    public void TestDotnetGetStarter()
    {
        using var driver = new RemoteWebDriver(remoteDriverURI, driverOptions.ToCapabilities());
        driver.Navigate().GoToUrl("https://dotnet.microsoft.com/");
        driver.FindElement(By.LinkText("Get Started")).Click();
        IWebElement nextLink = null;
        do
        {
            nextLink?.Click();
            nextLink = driver.FindElements(By.CssSelector(".step:not([style='display:none;']):not([style='display: none;']) .step-select")).FirstOrDefault();
        } while (nextLink != null);
        var lastStepTitle = driver.FindElement(By.CssSelector(".step:not([style='display:none;']):not([style='display: none;']) h2")).Text;
        Helper.takeSnapShot(driver, screenshotFolder+"/test1.png");
        Assert.AreEqual(lastStepTitle, "Next steps");    
    }

    [Test]
    public void TestGoogleSearch()
    {
        using var driver = new RemoteWebDriver(remoteDriverURI, driverOptions.ToCapabilities());
        driver.Navigate().GoToUrl("https://google.com/");
        IWebElement query = driver.FindElement(By.Name("q"));
        query.SendKeys("Get started dotNET");
        query.SendKeys(Keys.Enter);
        IWebElement correctResult = driver.FindElement(By.XPath("//*[text()='Get started with .NET - Microsoft Docs']"));
        Helper.takeSnapShot(driver, screenshotFolder+"/test2.png");
        Assert.NotNull(correctResult);     
    }
}